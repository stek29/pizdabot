FROM python:3.7.4-slim-stretch

RUN mkdir /bot
WORKDIR /bot

COPY requirements.txt .

# regex package has build deps
RUN apt-get update && apt-get install -y \
    build-essential \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN pip install -r requirements.txt

# remove build deps
RUN apt-get remove -y build-essential && apt autoremove -y

COPY bot.py ./
RUN chmod +x bot.py

ENTRYPOINT ["./bot.py"]