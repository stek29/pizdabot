#!/usr/bin/env python

from aiogram import Bot, Dispatcher, executor, types
from russtress import Accent
import random
import regex
import re
import asyncio
import sys
from config import *
import logging

logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(LOGGING_LEVEL)


def transliterate(s: str):
    return s.translate(
        str.maketrans('abcdefghijklmnoprstuvwyz', 'абцдефгхижклмнопрстуввйз')
    ).replace('x', 'кс')


DEDUPLICATE_REGEXP = re.compile(r'(.)\1+')


def deduplicate(s: str):
    return DEDUPLICATE_REGEXP.sub(r'\1', s)


MERGE_SINGLE_REGEXP = re.compile(r'(?:\b\w\b\s*){2,}')


def merge_single(s: str):
    return MERGE_SINGLE_REGEXP.sub(
        lambda m: m.group(0).replace(' ', ''),
        s)


accenter = Accent()


def pizdarhymes(words: str):
    if not words.endswith('да'):
        return False

    if words.endswith('пизда'):
        # don't reply to pizda with pizda :)
        return False

    stressed = accenter.put_stress(words).split()[-1]

    if stressed.count("'") == 0:
        # russtress doesn't put spell if there's only
        # one syllable
        return True

    return stressed.endswith("да'")


FILTER_ALPHASPACE_REGEX = regex.compile(r'[^\p{L}\s]')


def filter_alphaspace(s: str):
    return FILTER_ALPHASPACE_REGEX.sub(' ', s).strip()


def str_chain(funcs, s):
    for f in funcs:
        s = f(s)
    return s


# Initialize bot and dispatcher
bot = Bot(token=TOKEN)
dp = Dispatcher(bot)


@dp.message_handler()
@dp.edited_message_handler()
async def on_chat_message(msg: types.Message):
    msg_id = msg.message_id
    logger.debug('%d: New message: %s', msg_id, msg)

    text = msg.text.lower()
    if len(text) < 0:
        return

    words = str_chain((
        filter_alphaspace,
        transliterate,
        deduplicate,
        merge_single,
    ), text)

    if words.startswith("филя"):
        await bot.send_message(
            msg.chat.id,
            accenter.put_stress(words),
            reply_to_message_id=msg_id)

    if not pizdarhymes(words):
        logger.debug('%d: Doesnt rhyme (%s)', msg_id, words)
        return

    author = msg.from_user.id
    x = random.random()-0.6
    if x < 0:
        logger.debug('%d: Better luck next time (%d)', msg_id, x)
        return

    logger.info('%d: Going to Reply (sender: %d)', msg_id, author)
    await asyncio.sleep(0.5+15*x)

    if author == 115825833:
        stck = random.choice(ORESHKIN_STICKERS)
    else:
        stck = random.choice(STICKERS)

    await bot.send_sticker(chat_id=msg.chat.id, sticker=stck, reply_to_message_id=msg_id)


if __name__ == '__main__':
    executor.start_polling(dp)
