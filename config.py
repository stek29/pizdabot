import logging as _logging
from os import environ as _environ

STICKERS = [
    'CAADAgADJQoAAtA90gh4YyuhUAE_VQI',
    'CAADAgADJgoAAtA90gjEaaypS6f5UQI',
    'CAADAgADJwoAAtA90giq8QtF9KM-hAI',
    'CAADAgADKAoAAtA90ghNeZ0wNL-26wI',
    'CAADAgADKQoAAtA90ghEWrfp5S2PiAI',
    'CAADAgADKgoAAtA90giRFHq4kRxEtQI'
]

ORESHKIN_STICKERS = STICKERS + ['CAADAgADKwoAAtA90gjwnJHUe0rvGgI']*4

TOKEN = _environ['TOKEN']

LOGGING_LEVEL = _logging.INFO
